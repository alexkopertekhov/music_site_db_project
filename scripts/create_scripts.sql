CREATE SCHEMA music_database;

CREATE TABLE music_database.user
(
    user_id         SERIAL PRIMARY KEY,
    subscription_id INTEGER,
    name            VARCHAR(255) NOT NULL,
    data            DATE         NOT NULL
);

CREATE TABLE music_database.subscription
(
    subscription_id SERIAL PRIMARY KEY,
    price           INTEGER NOT NULL,
    time            INTEGER NOT NULL
);

CREATE TABLE music_database.track
(
    track_id  SERIAL PRIMARY KEY,
    name      VARCHAR(255) NOT NULL,
    artist_id INTEGER      NOT NULL,
    time      INTEGER      NOT NULL
);

CREATE TABLE music_database.playlist
(
    playlist_id SERIAL PRIMARY KEY,
    name        VARCHAR(255) NOT NULL,
    track_cnt   INTEGER CHECK ( track_cnt >= 0 )
);

CREATE TABLE music_database.artist
(
    artist_id SERIAL PRIMARY KEY,
    name      VARCHAR(255) NOT NULL
);

CREATE TABLE music_database.playlist_x_track
(
    playlist_id SERIAL NOT NULL,
    track_id    SERIAL NOT NULL,
    PRIMARY KEY (playlist_id, track_id),
    FOREIGN KEY (track_id)
        REFERENCES music_database.track (track_id),
    FOREIGN KEY (playlist_id)
        REFERENCES music_database.playlist (playlist_id)
);

CREATE TABLE music_database.playlist_x_user
(
    playlist_id SERIAL NOT NULL,
    user_id     SERIAL NOT NULL,
    PRIMARY KEY (playlist_id, user_id),
    FOREIGN KEY (playlist_id)
        REFERENCES music_database.playlist (playlist_id),
    FOREIGN KEY (user_id)
        REFERENCES music_database.user (user_id)
);

CREATE TABLE music_database.playlist_x_artist
(
    playlist_id SERIAL NOT NULL,
    artist_id   SERIAL NOT NULL,
    PRIMARY KEY (playlist_id, artist_id),
    FOREIGN KEY (playlist_id)
        REFERENCES music_database.playlist (playlist_id),
    FOREIGN KEY (artist_id)
        REFERENCES music_database.artist (artist_id)
);

CREATE TABLE music_database.subscription_x_user
(
    subscription_id SERIAL NOT NULL,
    user_id         SERIAL NOT NULL,
    PRIMARY KEY (user_id, subscription_id),
    FOREIGN KEY (subscription_id)
        REFERENCES music_database.subscription (subscription_id),
    FOREIGN KEY (user_id)
        REFERENCES music_database.user (user_id)
);

CREATE TABLE music_database.salary_artist
(
    artist_id INTEGER NOT NULL,
    date      DATE    NOT NULL,
    salary    INTEGER NOT NULL,
    FOREIGN KEY (artist_id)
        REFERENCES music_database.artist (artist_id)
);