INSERT INTO music_database.user (name, data, subscription_id)
VALUES ('Ivan Kuznetsov', '2023-04-19', 1),
       ('Petr Lisov', '2003-08-19', 1),
       ('Alkash(Aibulat Dirak)', '2023-04-08', 2),
       ('Maksim Sokolovskiy', '2022-08-05', 1),
       ('Platon Yadrov', '2023-03-07', 1),
       ('Yana Pigunova', '2005-06-06', 1),
       ('Victor Strebulaev', '2006-06-06', 2),
       ('Georgiy Kilinkarov', '2019-07-13', 1),
       ('Igor Filippenkov', '2023-04-21', 1),
       ('Vlad Bagranov', '2023-04-21', 1),
       ('Victor Bebra', '2023-04-01', 1),
       ('Victor Petrov', '2023-04-02', 2),
       ('Danila Shashkov', '2023-01-01', 1),
       ('Olga Dorushenkova', '2021-03-05', 2),
       ('Aleksandra Gerasimova', '2015-03-02', 2),
       ('Ivan Udin', '2017-03-03', 2),
       ('Ayan Isabekov', '2023-03-25', 1),
       ('Elizaveta Smirnova', '2022-09-01', 1),
       ('Danya Lyatorovskiy', '2014-05-05', 1),
       ('Grigoriy Konoplev', '2023-03-03', 1),
       ('Ilya Khozhaev', '2022-05-14', 2),
       ('Andrey Kostyanov', '2022-05-19', 2),
       ('Buzhan', '2021-12-12', 1),
       ('Tamara Udina', '2022-03-12', 2);


INSERT INTO music_database.artist (name)
VALUES ('Queen'),
       ('Nirvana'),
       ('Александр Розенбаум'),
       ('Олег Митяев'),
       ('Алёна Швец'),
       ('Звери');

INSERT INTO music_database.track (name, artist_id, time)
VALUES ('Зима',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Александр Розенбаум'), 240),
       ('Я Сижу Свет',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Александр Розенбаум'), 273),
       ('На Дону, На Доне',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Александр Розенбаум'), 245),
       ('Есаул',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Александр Розенбаум'), 291),
       ('Любовь и крыша',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Александр Розенбаум'), 230),
       ('Smells Like Teen Spirit',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Nirvana'), 254),
       ('Come As You Are',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Nirvana'), 219),
       ('Lithium',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Nirvana'), 257),
       ('In Bloom',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Nirvana'), 254),
       ('Heart-Shaped Box',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Nirvana'), 236),
       ('All Apologies',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Nirvana'), 231),
       ('Rape Me',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Nirvana'), 178),
       ('Dumb',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Nirvana'), 166),
       ('Polly',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Nirvana'), 177),
       ('About A Girl',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Nirvana'), 168),
       ('Bohemian Rhapsody',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Queen'), 354),
       ('We Will Rock You',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Queen'), 137),
       ('We Are The Champions',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Queen'), 180),
       ('Don`t Stop Me Now',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Queen'), 213),
       ('Somebody To Love',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Queen'), 295),
       ('Killer Queen',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Queen'), 182),
       ('Radio Ga Ga',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Queen'), 350),
       ('I Want To Break Free',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Queen'), 253),
       ('Under Pressure',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Queen'), 246),
       ('The Show Must Go On',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Queen'), 256),
       ('Мама',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Олег Митяев'), 218),
       ('В осеннем парке',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Олег Митяев'), 215),
       ('Вечная история',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Олег Митяев'), 185),
       ('Отец',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Олег Митяев'), 197),
       ('Царица непала',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Олег Митяев'), 213),
       ('Небесный калькулятор',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Олег Митяев'), 246),
       ('Питер',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Олег Митяев'), 254),
       ('Первое Свидание',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Алёна Швец'), 213),
       ('Мелкая с гитарой',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Алёна Швец'), 211),
       ('Портвейн',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Алёна Швец'), 197),
       ('Мемы',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Алёна Швец'), 199),
       ('Машина для Убийств',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Алёна Швец'), 236),
       ('Олимпос',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Алёна Швец'), 255),
       ('Скейтер',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Алёна Швец'), 243),
       ('До скорой встречи',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Звери'), 315),
       ('Рома, извини',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Звери'), 267),
       ('Районы-кварталы',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Звери'), 315),
       ('Брюнетки и Блондинки',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Звери'), 212),
       ('Прогулки',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Звери'), 189),
       ('Напитки покрепче',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Звери'), 244),
       ('Южная ночь',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Звери'), 267),
       ('Киборг убийца',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Звери'), 296),
       ('Все что касается',
        (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Звери'), 275);

INSERT INTO music_database.subscription (price, time)
VALUES (199, 31),
       (1999, 365);

--заполнение плэйлистов по исполнителям
INSERT INTO music_database.playlist (name, track_cnt)
VALUES ('Звери', (SELECT count(track_id)
                  FROM music_database.track
                  WHERE artist_id = (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Звери'))),
       ('Queen', (SELECT count(track_id)
                  FROM music_database.track
                  WHERE artist_id = (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Queen'))),
       ('Nirvana', (SELECT count(track_id)
                    FROM music_database.track
                    WHERE artist_id =
                          (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Nirvana'))),
       ('Алёна_Швец_The_Best', (SELECT count(track_id)
                                FROM music_database.track
                                WHERE artist_id = (SELECT artist.artist_id
                                                   FROM music_database.artist
                                                   WHERE artist.name = 'Алёна Швец'))),
       ('Олег_Митяев_Лучшее', (SELECT count(track_id)
                               FROM music_database.track
                               WHERE artist_id = (SELECT artist.artist_id
                                                  FROM music_database.artist
                                                  WHERE artist.name = 'Олег Митяев'))),
       ('Александр_Розенбаум_Лучшее', (SELECT count(track_id)
                                       FROM music_database.track
                                       WHERE artist_id = (SELECT artist.artist_id
                                                          FROM music_database.artist
                                                          WHERE artist.name = 'Александр Розенбаум')));

INSERT INTO music_database.playlist_x_track(playlist_id, track_id)
SELECT ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Звери')), track_id
FROM music_database.track
WHERE artist_id = (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Звери');

INSERT INTO music_database.playlist_x_track(playlist_id, track_id)
SELECT ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Queen')), track_id
FROM music_database.track
WHERE artist_id = (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Queen');

INSERT INTO music_database.playlist_x_track(playlist_id, track_id)
SELECT ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Nirvana')), track_id
FROM music_database.track
WHERE artist_id = (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Nirvana');

INSERT INTO music_database.playlist_x_track(playlist_id, track_id)
SELECT ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Алёна_Швец_The_Best')), track_id
FROM music_database.track
WHERE artist_id = (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Алёна Швец');

INSERT INTO music_database.playlist_x_track(playlist_id, track_id)
SELECT ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Олег_Митяев_Лучшее')), track_id
FROM music_database.track
WHERE artist_id = (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Олег Митяев');

INSERT INTO music_database.playlist_x_track(playlist_id, track_id)
SELECT ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Александр_Розенбаум_Лучшее')),
       track_id
FROM music_database.track
WHERE artist_id = (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Александр Розенбаум');

INSERT INTO music_database.playlist_x_artist (artist_id, playlist_id)
VALUES ((SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Звери'),
        (SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Звери')),
       ((SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Queen'),
        (SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Queen')),
       ((SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Nirvana'),
        (SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Nirvana')),
       ((SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Алёна Швец'),
        (SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Алёна_Швец_The_Best')),
       ((SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Олег Митяев'),
        (SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Олег_Митяев_Лучшее')),
       ((SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Александр Розенбаум'),
        (SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Александр_Розенбаум_Лучшее'));


--заполнение плэйлистов по жанрам
INSERT INTO music_database.playlist (name)
VALUES ('Rock'),
       ('Author ''s song'),
       ('Indie-pop');

INSERT INTO music_database.playlist_x_track(playlist_id, track_id)
SELECT ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Rock')), track_id
FROM music_database.track
WHERE artist_id IN
      (SELECT artist.artist_id
       FROM music_database.artist
       WHERE artist.name = 'Звери'
          OR artist.name = 'Queen'
          OR artist.name = 'Nirvana');

INSERT INTO music_database.playlist_x_track(playlist_id, track_id)
SELECT ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Author ''s song')),
       track_id
FROM music_database.track
WHERE artist_id IN (SELECT artist.artist_id
                    FROM music_database.artist
                    WHERE artist.name = 'Олег Митяев'
                       OR artist.name = 'Александр Розенбаум');

INSERT INTO music_database.playlist_x_track(playlist_id, track_id)
SELECT ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Indie-pop')),
       track_id
FROM music_database.track
WHERE artist_id = (SELECT artist.artist_id FROM music_database.artist WHERE artist.name = 'Алёна Швец');

--пример создания личных плейлистов слушателей и их заполнение
INSERT INTO music_database.playlist (name)
VALUES ('Aibulat_playlist'),
       ('Pigunova_playlist'),
       ('Lisov_playlist');

INSERT INTO music_database.playlist_x_track(playlist_id, track_id)
VALUES ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Pigunova_playlist'),
        (SELECT track.track_id FROM music_database.track WHERE name = 'Напитки покрепче')),
       ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Pigunova_playlist'),
        (SELECT track.track_id FROM music_database.track WHERE name = 'Олимпос')),
       ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Pigunova_playlist'),
        (SELECT track.track_id FROM music_database.track WHERE name = 'Мелкая с гитарой')),
       ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Pigunova_playlist'),
        (SELECT track.track_id FROM music_database.track WHERE name = 'Rape Me')),
       ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Pigunova_playlist'),
        (SELECT track.track_id FROM music_database.track WHERE name = 'Питер')),
       ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Pigunova_playlist'),
        (SELECT track.track_id FROM music_database.track WHERE name = 'Машина для Убийств'));

INSERT INTO music_database.playlist_x_track(playlist_id, track_id)
VALUES ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Aibulat_playlist'),
        (SELECT track.track_id FROM music_database.track WHERE name = 'Киборг убийца')),
       ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Aibulat_playlist'),
        (SELECT track.track_id FROM music_database.track WHERE name = 'Напитки покрепче')),
       ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Aibulat_playlist'),
        (SELECT track.track_id FROM music_database.track WHERE name = 'Портвейн')),
       ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Aibulat_playlist'),
        (SELECT track.track_id FROM music_database.track WHERE name = 'Есаул')),
       ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Aibulat_playlist'),
        (SELECT track.track_id FROM music_database.track WHERE name = 'Я Сижу Свет')),
       ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Aibulat_playlist'),
        (SELECT track.track_id FROM music_database.track WHERE name = 'На Дону, На Доне'));

INSERT INTO music_database.playlist_x_track(playlist_id, track_id)
VALUES ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Lisov_playlist'),
        (SELECT track.track_id FROM music_database.track WHERE name = 'Killer Queen')),
       ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Lisov_playlist'),
        (SELECT track.track_id FROM music_database.track WHERE name = 'We Will Rock You')),
       ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Lisov_playlist'),
        (SELECT track.track_id FROM music_database.track WHERE name = 'Come As You Are')),
       ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Lisov_playlist'),
        (SELECT track.track_id FROM music_database.track WHERE name = 'Dumb')),
       ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Lisov_playlist'),
        (SELECT track.track_id FROM music_database.track WHERE name = 'Bohemian Rhapsody')),
       ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Lisov_playlist'),
        (SELECT track.track_id FROM music_database.track WHERE name = 'Don`t Stop Me Now'));

INSERT INTO music_database.playlist_x_user(playlist_id, user_id)
VALUES ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Pigunova_playlist'),
        (SELECT "user".user_id FROM music_database.user WHERE name = 'Yana Pigunova')),
       ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Aibulat_playlist'),
        (SELECT "user".user_id FROM music_database.user WHERE name = 'Alkash(Aibulat Dirak)')),
       ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Lisov_playlist'),
        (SELECT "user".user_id FROM music_database.user WHERE name = 'Petr Lisov'));

INSERT INTO music_database.playlist_x_user(playlist_id, user_id)
VALUES ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Rock'),
        (SELECT "user".user_id FROM music_database.user WHERE name = 'Ivan Kuznetsov')),
       ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Rock'),
        (SELECT "user".user_id FROM music_database.user WHERE name = 'Petr Lisov')),
       ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Rock'),
        (SELECT "user".user_id FROM music_database.user WHERE name = 'Platon Yadrov')),
       ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Rock'),
        (SELECT "user".user_id FROM music_database.user WHERE name = 'Victor Bebra')),
       ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Rock'),
        (SELECT "user".user_id FROM music_database.user WHERE name = 'Ivan Udin')),
       ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Rock'),
        (SELECT "user".user_id FROM music_database.user WHERE name = 'Danya Lyatorovskiy')),
       ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Rock'),
        (SELECT "user".user_id FROM music_database.user WHERE name = 'Grigoriy Konoplev')),
       ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Rock'),
        (SELECT "user".user_id FROM music_database.user WHERE name = 'Ilya Khozhaev')),
       ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Rock'),
        (SELECT "user".user_id FROM music_database.user WHERE name = 'Andrey Kostyanov')),
       ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Rock'),
        (SELECT "user".user_id FROM music_database.user WHERE name = 'Tamara Udina')),
       ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Author ''s song'),
        (SELECT "user".user_id FROM music_database.user WHERE name = 'Alkash(Aibulat Dirak)')),
       ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Author ''s song'),
        (SELECT "user".user_id FROM music_database.user WHERE name = 'Maksim Sokolovskiy')),
       ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Author ''s song'),
        (SELECT "user".user_id FROM music_database.user WHERE name = 'Yana Pigunova')),
       ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Author ''s song'),
        (SELECT "user".user_id FROM music_database.user WHERE name = 'Victor Strebulaev')),
       ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Author ''s song'),
        (SELECT "user".user_id FROM music_database.user WHERE name = 'Georgiy Kilinkarov')),
       ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Author ''s song'),
        (SELECT "user".user_id FROM music_database.user WHERE name = 'Olga Dorushenkova')),
       ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Author ''s song'),
        (SELECT "user".user_id FROM music_database.user WHERE name = 'Elizaveta Smirnova')),
       ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Author ''s song'),
        (SELECT "user".user_id FROM music_database.user WHERE name = 'Ilya Khozhaev')),
       ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Author ''s song'),
        (SELECT "user".user_id FROM music_database.user WHERE name = 'Buzhan')),
       ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Indie-pop'),
        (SELECT "user".user_id FROM music_database.user WHERE name = 'Igor Filippenkov')),
       ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Indie-pop'),
        (SELECT "user".user_id FROM music_database.user WHERE name = 'Vlad Bagranov')),
       ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Indie-pop'),
        (SELECT "user".user_id FROM music_database.user WHERE name = 'Victor Petrov')),
       ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Indie-pop'),
        (SELECT "user".user_id FROM music_database.user WHERE name = 'Danila Shashkov')),
       ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Indie-pop'),
        (SELECT "user".user_id FROM music_database.user WHERE name = 'Olga Dorushenkova')),
       ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Indie-pop'),
        (SELECT "user".user_id FROM music_database.user WHERE name = 'Aleksandra Gerasimova')),
       ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Indie-pop'),
        (SELECT "user".user_id FROM music_database.user WHERE name = 'Ivan Udin')),
       ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Indie-pop'),
        (SELECT "user".user_id FROM music_database.user WHERE name = 'Ayan Isabekov')),
       ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Indie-pop'),
        (SELECT "user".user_id FROM music_database.user WHERE name = 'Tamara Udina')),
       ((SELECT playlist.playlist_id FROM music_database.playlist WHERE name = 'Indie-pop'),
        (SELECT "user".user_id FROM music_database.user WHERE name = 'Ilya Khozhaev'));

INSERT INTO music_database.salary_artist (artist_id, date, salary)
VALUES (1, '2022-01-01', 500),
       (2, '2022-01-01', 600),
       (3, '2022-01-01', 700),
       (4, '2022-01-01', 800),
       (5, '2022-01-01', 900),
       (6, '2022-01-01', 1000),
       (1, '2022-02-01', 550),
       (2, '2022-02-01', 650),
       (3, '2022-02-01', 750),
       (4, '2022-02-01', 850),
       (5, '2022-02-01', 950),
       (6, '2022-02-01', 1050),
       (1, '2022-03-01', 600),
       (2, '2022-03-01', 700),
       (3, '2022-03-01', 800),
       (4, '2022-03-01', 900),
       (5, '2022-03-01', 1000),
       (6, '2022-03-01', 1100),
       (1, '2022-04-01', 650),
       (2, '2022-04-01', 750),
       (3, '2022-04-01', 850),
       (4, '2022-04-01', 950),
       (5, '2022-04-01', 1050),
       (6, '2022-04-01', 1150),
       (1, '2022-05-01', 700),
       (2, '2022-05-01', 800),
       (3, '2022-05-01', 900),
       (4, '2022-05-01', 1000),
       (5, '2022-05-01', 1100),
       (6, '2022-05-01', 1200),
       (1, '2022-06-01', 750),
       (1, '2020-01-01', 1000),
       (2, '2020-01-01', 1200),
       (3, '2020-01-01', 800),
       (4, '2020-01-01', 1500),
       (5, '2020-01-01', 900),
       (6, '2020-01-01', 1100),
       (1, '2020-02-01', 1100),
       (2, '2020-02-01', 1300),
       (3, '2020-02-01', 900),
       (4, '2020-02-01', 1600),
       (5, '2020-02-01', 1000),
       (6, '2020-02-01', 1200),
       (1, '2020-03-01', 1200),
       (2, '2020-03-01', 1400),
       (3, '2020-03-01', 1000),
       (4, '2020-03-01', 1700),
       (5, '2020-03-01', 1100),
       (6, '2020-03-01', 1300),
       (1, '2020-04-01', 1300),
       (2, '2020-04-01', 1500),
       (3, '2020-04-01', 1100),
       (4, '2020-04-01', 1800),
       (5, '2020-04-01', 1200),
       (6, '2020-04-01', 1400),
       (1, '2020-05-01', 1400),
       (2, '2020-05-01', 1600),
       (3, '2020-05-01', 1200),
       (4, '2020-05-01', 1900),
       (5, '2020-05-01', 1300),
       (6, '2020-05-01', 1500);

INSERT INTO music_database.subscription_x_user (subscription_id, user_id)
SELECT subscription_id, user_id
FROM music_database.user;

UPDATE music_database.playlist
SET track_cnt = (SELECT count(track_id)
                 FROM music_database.playlist_x_track
                 WHERE playlist_id =
                       (SELECT music_database.playlist.playlist_id FROM music_database.playlist WHERE name = 'Rock'))
WHERE playlist_id = (SELECT music_database.playlist.playlist_id FROM music_database.playlist WHERE name = 'Rock');

UPDATE music_database.playlist
SET track_cnt = (SELECT count(track_id)
                 FROM music_database.playlist_x_track
                 WHERE playlist_id =
                       (SELECT music_database.playlist.playlist_id
                        FROM music_database.playlist
                        WHERE name = 'Author ''s song'))
WHERE playlist_id =
      (SELECT music_database.playlist.playlist_id FROM music_database.playlist WHERE name = 'Author ''s song');

UPDATE music_database.playlist
SET track_cnt = (SELECT count(track_id)
                 FROM music_database.playlist_x_track
                 WHERE playlist_id =
                       (SELECT music_database.playlist.playlist_id
                        FROM music_database.playlist
                        WHERE name = 'Indie-pop'))
WHERE playlist_id = (SELECT music_database.playlist.playlist_id FROM music_database.playlist WHERE name = 'Indie-pop');

UPDATE music_database.playlist
SET track_cnt = (SELECT count(track_id)
                 FROM music_database.playlist_x_track
                 WHERE playlist_id =
                       (SELECT music_database.playlist.playlist_id
                        FROM music_database.playlist
                        WHERE name = 'Aibulat_playlist'))
WHERE playlist_id =
      (SELECT music_database.playlist.playlist_id FROM music_database.playlist WHERE name = 'Aibulat_playlist');

UPDATE music_database.playlist
SET track_cnt = (SELECT count(track_id)
                 FROM music_database.playlist_x_track
                 WHERE playlist_id =
                       (SELECT music_database.playlist.playlist_id
                        FROM music_database.playlist
                        WHERE name = 'Pigunova_playlist'))
WHERE playlist_id =
      (SELECT music_database.playlist.playlist_id FROM music_database.playlist WHERE name = 'Pigunova_playlist');

UPDATE music_database.playlist
SET track_cnt = (SELECT count(track_id)
                 FROM music_database.playlist_x_track
                 WHERE playlist_id =
                       (SELECT music_database.playlist.playlist_id
                        FROM music_database.playlist
                        WHERE name = 'Lisov_playlist'))
WHERE playlist_id =
      (SELECT music_database.playlist.playlist_id FROM music_database.playlist WHERE name = 'Lisov_playlist');
